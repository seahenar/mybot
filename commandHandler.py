import config
from datetime import datetime, timedelta
from message import Message
import irc
import ircCommands
import regex
import statsHandler
import channelTracker
import userTracker
import handler
import scheduler
from handlers.handlerBase import HandlerBase
from validation import Validation
from context import ContextAware, ContextManager, RequiredProperty

def handle(command):
	if command.startswith('PING'):
		irc.handlePingRaw(command)
	else:
		if 'logger' in config.handlers:
			getHandler('logger').log(command)

		sender = command.split('!')[0].replace(':','')
		components = command.split()
		senderHostname = components[0].split('!')
		if len(senderHostname) > 1:
			senderHostname = '*!' + senderHostname[1]
		else:
			senderHostname = senderHostname[0].replace(':','')
		action = ''
		destination = ''
		text = ''
		if len(components) > 3:
			text = ' '.join(components[3:])[1:]
		if len(components) > 2:
			destination = components[2]
			if destination.startswith(':'):
				destination = destination[1:]
		if len(components) > 1:
			action = components[1]

		message = Message(sender, destination, text)

		if action == 'PRIVMSG' and text.strip() != '':
			handlePrivMsg(message)

		if 'topic' in config.handlers and (action == 'TOPIC' or action == '332'):
			if action == '332':
				destination = components[3]
				text = ' '.join(components[4:])[1:]
			getHandler('topic').setTopic(destination, text)
		
		if action == '353':
			components[5] = components[5].replace(':', '')
			userListInChannel = components[5:]
			channelTracker.botJoined(components[4], userListInChannel)
			if components[4] == config.channel:
				userTracker.botJoined(userListInChannel)

		if action == 'NICK':
			userTracker.userChangedName(sender, components[2])
			channelTracker.userChangedName(sender, components[2])

		if action == 'QUIT':
			quitMessage = ' '.join(components[2:])[1:]
			handleQuitAction(message, senderHostname, quitMessage)

		if action == 'JOIN':
			if destination == config.channel:
				userTracker.userJoined(sender)
			if sender != config.nick:
				channelTracker.userJoined(destination, sender)

			if destination in config.autoAssignLevels:
				autoLevel = config.getAutoLevel(sender, destination)
				if autoLevel != None:
					if isAuthedUser(sender):
						ircCommands.sendCommand(sender, destination, autoLevel, True)

		if action == 'PART':
			if destination == config.channel:
				userTracker.userLeft(sender, 'parted the channel')
			channelTracker.userLeft(destination, sender)

		if action == 'KICK':
			if components[3] == config.nick:
				irc.send('JOIN ' + destination)
			if destination == config.channel:
				userTracker.userLeft(components[3], 'were kicked from the channel')
			channelTracker.userLeft(destination, components[3])

		if action == 'MODE' and destination == config.channel:
			changes = components[3]
			people = components[4:]
			changeIndex = 0
			give = ''
			for person in people:
				if changes[changeIndex] in ['+', '-']:
					give = changes[changeIndex]
					changeIndex += 1
				userTracker.modeChanged(person, give + changes[changeIndex])
				changeIndex += 1

def handleQuitAction(message, senderHostname, quitMessage):
	upperText = quitMessage.upper()
	if upperText.find('TIMEOUT') != -1 or upperText.find('RESET BY PEER') != -1 or upperText.find('BROKEN PIPE') != -1:
		numSuccessiveTimeouts = statsHandler.userTimedOut(message.sender)
		if numSuccessiveTimeouts > 1:
			handleSuccessiveTimeouts(message.sender, senderHostname, numSuccessiveTimeouts)
		channelTracker.userQuit(message.sender)
		userTracker.userLeft(message.sender, 'timed out')
		return

	channelTracker.userQuit(message.sender)
	userTracker.userLeft(message.sender, 'quit IRC')

def handlePrivMsg(message):
	if message.destination in config.floodPreventionChannels:
		userTracker.updateLastSeen(message.sender, message.text, checkForSpam = True)
	else:
		userTracker.updateLastSeen(message.sender, '', checkForSpam = False)

	if message.destination != config.nick:
		channelTracker.messageReceived(message)
		if regex.isDangerMessage(message.text):
			irc.sendMsg(message.destination, u'\u000304\u0002Caution:\u0002\u0003 ' + message.sender + '\'s link above may be NSFW')

	handleAction(message)

def handleSuccessiveTimeouts(name, hostname, numSuccessiveTimeouts):
	if numSuccessiveTimeouts >= 7:
		spamProtectedChannelsUserIsIn = channelTracker.getSpamProtectedChannelsUserIsIn(name)

		for channel in spamProtectedChannelsUserIsIn:
			irc.inform(channel, name + u'\'s tubes are clogged')
			irc.sendMsg(name, 'Please fix your internet connection, you\'re clogging the channel with timeout messages! You will be able to rejoin the channel again in 60 minutes, or just PM me "fixed" (/msg %s fixed) and you can rejoin immediately.' % config.nick)
			TempBanWithAppeal(name, channel, hostname, datetime.now() + timedelta(minutes=60))

def handleAction(message):
	if not message.isBotCommand():
		userTracker.tryHandleOpenContext(message.sender, message.text)
		return

	levelsForUser = None
	if message.sender == config.masterAdmin:
		levelsForUser = handler.getAllLevels()
	else:
		levelsForUser = HandlerBase.getLevelsForUser(message.sender)

	commandLevel = handler.getLevelUserCanRunCommandAt(message.command, levelsForUser)
	if commandLevel != None:
		if commandLevel != config.allLevel:
			authedUser = isAuthedUser(message.sender)
			if not authedUser:
				irc.sendMsg(message.sender, 'You must be authorised with nickserv to use this ' + commandLevel + ' command')
				return
		handler.handle(commandLevel, message.command, message)
		return

	# if we have got here, then it might be a list command. let's find out
	if 'lists' in config.handlers:
		getHandler('lists').handleList(message)

def handleQuit(message):
	quitReason = 'admin shutdown'
	if message.numArgs >= 1:
		quitReason = ' '.join(message.args)

	irc.send('QUIT :' + quitReason)
	irc.disconnect()
	if 'logger' in config.handlers:
		getHandler('logger').logBotQuit(quitReason)
		getHandler('logger').stop()
	exit()

def getHandler(name):
	return handler.getGlobalHandler(name)

def isAuthedUser(user):
	return userTracker.isAuthedUser(user)

class TempBanWithAppeal(ContextAware):
	def __init__(self, username, channel, hostname, banExpiryTime):
		userTracker.tempbanHostname(username, channel, hostname, banExpiryTime)
		scheduler.addEvent(banExpiryTime, [ContextManager.deleteContext, username, None])

		ContextAware.__init__(self, username)
		ContextManager.setContext(username, self)

		self.banExpiryTime = banExpiryTime

		self.requiredProperties = [
			RequiredProperty('fixed', 'Is your internet fixed yet?', Validation.fixed),
		]

	def onComplete(self):
		irc.sendMsg(self.username, 'Great! Welcome back to the internet!')
		userTracker.cancelTempBan(self.username, self.banExpiryTime)
		scheduler.removeScheduledEvent(self.banExpiryTime, [ContextManager.deleteContext, self.username, None])
