from types import StringType
import imp
import os
import sys
import traceback

import config
import irc

handlers = {}
globalHandlers = {}
pluginToHandlerMapping = {}

def addHandler(levels, command):
	if currentLoadingPlugin in pluginToHandlerMapping:
		pluginToHandlerMapping[currentLoadingPlugin].append(command.trigger)
	else:
		pluginToHandlerMapping[currentLoadingPlugin] = [command.trigger]

	print '\tloaded', command.trigger,
	if type(levels) == StringType:
		levels = [levels]

	print 'at levels: ' + ', '.join(levels)
	for level in levels:
		if level in handlers:
			handlers[level][command.trigger] = command
		else:
			handlers[level] = dict([(command.trigger, command)])

def registerGlobalHandler(key, handlr):
	globalHandlers[key] = handlr

def getGlobalHandler(key):
	if key in globalHandlers.keys():
		return globalHandlers[key]

def getLevelUserCanRunCommandAt(command, userLevels):
	for level in userLevels:
		if level in handlers.keys() and command in handlers[level]:
			return level
	
	if command in handlers[config.allLevel]:
		return config.allLevel

	return None

def getAllLevels():
	return handlers.keys()

def handle(level, command, message):
	try:
		handlers[level][command].handler(message)
	except Exception, e:
		irc.sendMultilineMsg(config.masterAdmin, ['FATAL. %s => %s => %s at level %s with args:' % (message.sender, message.destination, command, level), message.text, traceback.format_exc().replace('\n', '__')])

def getCommandsForLevel(level):
	if level in handlers.keys():
		return handlers[level].keys()
	return []

def isCoreModule(moduleName):
	return os.path.exists(moduleName + '.py') or os.path.exists(moduleName + '.pyc')

def reloadCoreModule(moduleName):
	filename = moduleName + '.py'
	try:
		if os.path.exists(filename):
			imp.load_source(moduleName, filename)
		else:
			filename = moduleName + '.pyc'
			imp.load_compiled(moduleName, filename)
	except Exception, e:
		print e
		return e

def getInfoForCommandAtLevel(command, level):
	return handlers[level][command]

currentLoadingPlugin = None
def loadPlugin(handler, arguments):
	global currentLoadingPlugin
	currentLoadingPlugin = handler
	moduleName = config.handlerDirectory + '.' + handler
	modulePath = config.handlerDirectory + '/' + handler + '.py'
	print 'loading', handler
	try:
		if os.path.exists(modulePath):
			module = imp.load_source(moduleName, modulePath)
		else:
			modulePath = config.handlerDirectory + '/' + moduleName + '.pyc'
			module = imp.load_compiled(moduleName, modulePath)
		module.__init__(arguments)
	except Exception, e:
		print 'Error loading module', moduleName
		print traceback.format_exc()
		return e

def loadPlugins():
	for handler, arguments in config.handlers.iteritems():
		if loadPlugin(handler, arguments) != None:
			exit(1)

def unloadPlugin(pluginName):
	if pluginName in pluginToHandlerMapping:
		commandsToRemove = pluginToHandlerMapping[pluginName]
		for level in handlers:
			for command in commandsToRemove:
				if command in handlers[level]:
					del handlers[level][command]
					print 'removing \'' + command + '\' from level \'' + level + '\''
		return True
	else:
		return False

listeners = {}
def listen(action, handler):
	if action in listeners:
		listeners[action].append(handler)
	else:
		listeners[action] = [handler]

def fire(action):
	if action in listeners:
		for handler in listeners[action]:
			handler()
	else:
		print 'WARNING: action ', action, 'being fired has no listeners'
