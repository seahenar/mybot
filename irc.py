from datetime import datetime, timedelta
import socket
import config
import time
import timeUtils
import channelTracker
import userTracker
import mongler
import handler
import scheduler

irc = socket.socket()
receiveQueue = []
checkConnection = 5
lastReceive = None

def connect() :
	print 'Connecting to %s on port %s' % (config.network, config.port)
	global irc

	try:
		bindAddress = getattr(config, 'bindAddress', '')
		irc = socket.create_connection((config.network, config.port), 5, (bindAddress, 0))

		if config.ssl:
			irc = socket.ssl(irc)
			print 'SSL enabled. ',
		print 'Connected to %s' % (config.network,),

		family = getattr(irc, 'family', None)
		if family == socket.AF_INET:
			print 'using IPv4'
		elif family == socket.AF_INET6:
			print 'using IPv6'

	except Exception, e:
		print e
		print 'connect failed. waiting for 30 seconds before retry'
		reconnect()
		return

	handlePing()

	if config.serverPassword != '':
		send('PASS ' + config.serverPassword)
		handlePing()

	send('NICK ' + config.nick)
	handlePing()

	send('USER ' + config.userinfo)
	handlePing()
	handlePing()

	send('PRIVMSG NickServ identify ' + config.identString)
	if handlePing():
		send('PRIVMSG NickServ identify ' + config.identString)

	handlePing()
	for chan in config.channelsToJoin:
		send('JOIN ' + chan)
		channelTracker.join(chan)

	print 'Initalised.'

	if not config.testing:
		handler.fire('onConnected')

def handlePing():
	connectMessage = receive(False)
	print connectMessage
	for message in connectMessage.split('\r\n'):
		if message.startswith('PING'):
			handlePingRaw(message)
			return True
	return False

def handlePingRaw(pongQuery):
	send('PONG ' + ''.join(pongQuery.split()[1:]))

def disconnect():
	try:
		irc.close()
	except:
		pass

def reconnect():
	disconnect()
	time.sleep(30)
	connect()
	
def send(message):
	if not message.endswith('\r\n'):
		message += '\r\n'
	if config.debug:
		print 'SENDING: ' + message,

	message = message.encode('utf-8')
	try:
		if config.ssl:
			irc.write(message)
		else:
			irc.send(message)
	except socket.error, e:
		e = str(e)
		print 'send error: ' + e
		reconnect()
	except Exception, e:
		print 'send except: ' + str(e)
	
def receive(useQueue = True):
	global receiveQueue
	global lastReceive
	try:
		if config.ssl:
			data = irc.read(8192)
		else:
			data = irc.recv(8192)

		data = decode(data)
		if data == None:
			return ''
		if data == '':
			print 'received empty string. assuming socket dead. reconnecting in 30 seconds'
			reconnect()
			return ''
		lastReceive = datetime.now()
		if useQueue and len(receiveQueue) > 0:
			data = data + '\r\n'.join(receiveQueue)
			receiveQueue = []
		return data
	except socket.timeout, e:
		if lastReceive != None:
			if (lastReceive + timedelta(minutes=checkConnection)) < datetime.now():
				print 'assuming dropped by server. reconnecting'
				reconnect()
		return ''
	except socket.error, e:
		e = str(e)
		if e == 'The read operation timed out':
			return ''
		print 'error: ' + e
		reconnect()
		return ''
	except Exception, e:
		print 'except: ' + str(e)
		return ''

def decode(data):
	try:
	 	return data.decode('utf-8')
	except:
		try:
			return data.decode('CP1252')
		except:
			print 'I can\'t decode this!'
			return None

def inform(destination, message, target = ''):
	sendMsg(destination, u'\u0002Information' + target + u':\u0002 ' + message) 

def sendMsg(destination, content, msgType = None, evenIfOffline = False):
	sendMultilineMsg(destination, [content], msgType, evenIfOffline)

def sendMultilineMsg(destination, messages, msgType = None, evenIfOffline = False):
	msgPref = 'PRIVMSG'
	if 'userPrefs' in config.handlers:
		if msgType == None:
			msgPref = getHandler('userPrefs').getMsgPref('msg', destination)
		else:
			msgPref = getHandler('userPrefs').getMsgPref(msgType, destination)

	commands = []
	for message in messages:
		shouldMongle = False

		if destination.startswith('#'):
			shouldMongle = mongler.shouldMongle()
			if 'logger' in config.handlers:
				getHandler('logger').logBotMessage(message)
		else:
			if not userTracker.isOnline(destination):
				if evenIfOffline:
					handler.getGlobalHandler('tell').addTell(config.nick, datetime.now(), destination, message)
				elif config.debug:
					print 'not sending \'', message, '\' to', destination, 'reason: offline'
					return
			
		message = mongler.colourize(message)
		if shouldMongle:
			message = mongler.mongle(message)
			correction = message[1]
			message = message[0]
			if correction != None:
				scheduler.addEvent(datetime.now() + timedelta(milliseconds=750 +100 * len(correction)), [sendMsg, destination, correction, msgType])

		commands.append(msgPref + ' ' + destination + ' :' + message)

	send('\r\n'.join(commands))

def isAuthedUser(user):
	if config.testing:
		return True
	else:
		return tryProcessAuthedUser(user, 0)

def tryProcessAuthedUser(user, count):
	if count == 0:
		send('WHOIS ' + user)

	if count > config.maxAuthTries:
		if config.debug:
			print user + ' auth failed'
		return False
	
	if config.debug:
		print 'authorising ' + user + '... ',

	response = receive(False)
	response = response.split('\r\n')
	for resp in response:
		parts = resp.split()
		if len(parts) > 1:
			if parts[1] == '307':
				components = resp.split(':')
				text = ':'.join(components[2:])
				if text == 'is a registered nick' or text == 'is identified for this nick':
					if config.debug:
						print user + ' authorised'
					return True
			elif parts[1] == '330':
				components = resp.split(':')
				text = ':'.join(components[2:])
				if text == 'is logged in as' and parts[4].lower() == user:
					if config.debug:
						print user + ' authorised'
					return True
		global receiveQueue
		receiveQueue.append(resp)

	return tryProcessAuthedUser(user, count + 1)

def getHostname(user):
	send('WHOIS ' + user)
	return tryGetHostname(user, 0)

def tryGetHostname(user, count):
	if count > config.maxAuthTries:
		return None

	response = receive(False)
	response = response.split('\r\n')
	for resp in response:
		parts = resp.split()
		if len(parts) > 1:
			if parts[1] == '311' and len(parts) >= 6:
				return parts[4] + '@' + parts[5]
		global receiveQueue
		receiveQueue.append(resp)
	return tryGetHostname(user, count + 1)

def kickUser(user, channel, reason):
	send('KICK ' + channel + ' ' + user + ' :' + reason)

def getHandler(name):
	return handler.getGlobalHandler(name)
