import irc

def admin(user, channel, give):
	sendCommand(user, channel, 'a', give)

def op(user, channel, give):
	sendCommand(user, channel, 'o', give)

def voice(user, channel, give):
	sendCommand(user, channel, 'v', give)

def ban(user, channel, give):
	sendCommand(user, channel, 'b', give)

def hop(user, channel, give):
	sendCommand(user, channel, 'h', give)

def sendCommand(user, channel, command, give):
	mode = ' +' + command + ' ' if give else ' -' + command + ' '
	irc.send('MODE ' + channel + mode + user)
