import config
import irc
from datetime import datetime, timedelta
import traceback

scheduledEvents = []

def doScheduledEvents():
	timeNow = datetime.now()
	i = 0
	numEvents = len(scheduledEvents)
	while i < numEvents:
		event = scheduledEvents[i]
		timeDue = event[0]
		if timeDue < timeNow:
			args = []
			if len(event) > 1:
				args = event[2:]

			try:
				event[1](*args)
			except Exception, e:
				irc.sendMultilineMsg(config.masterAdmin, ['FATAL. scheduled event invocation %s failed' % str(event[1]), str(e)])

			scheduledEvents.remove(event)
			numEvents = len(scheduledEvents)
			continue
		i += 1

def addEvent(timeDue, args):
	scheduledEvents.append([timeDue] + args)

def removeScheduledEvent(timeDue, args):
	for event in scheduledEvents:
		if event[0] == timeDue:
			for i in range(0, len(args)):
				if event[1 + i] != args[i]:
					break
			scheduledEvents.remove(event)
			return True
	print 'failed to remove item from scheduler'
	return False
