#!/usr/bin/python
import argparse
parser = argparse.ArgumentParser(description = 'IRC bot')
parser.add_argument('-c', dest = 'configFile', required = True, help = 'The path to your config file')
parser.add_argument('--init-missing-database', action = 'store_true', help = 'If the configured database file does not exist, a new database will be created and its schema initialised')
args = parser.parse_args()

import config
config.loadUserConfig(args.configFile)

import time, os
print 'Server local time is',
print time.strftime('%X %x %Z')
customTimezone = getattr(config, 'timezoneOverride', '')
if customTimezone == '':
	print 'No timezoneOverride specified in config'
else:
	os.environ['TZ'] = customTimezone
	time.tzset()
	print 'Changing bot timezone to',
	print time.strftime('%X %x %Z')

import database
database.initialise(args)
database.applyMigrations()

import traceback
import commandHandler
import irc
import scheduler
import handler

handler.loadPlugins()

irc.connect()

while True:
#	if config.debug:
#		print 'pre receive'
	data = irc.receive()
#	if config.debug:
#		print 'post receive'

	dataArray = data.split('\r\n')[0:-1]
	scheduler.doScheduledEvents()

	for message in dataArray:
		if message != '':
			if config.debug:
				print 'RECEIVED: '+ message

			try:
				commandHandler.handle(message)
			except Exception, e:
				print e
				irc.sendMultilineMsg(config.masterAdmin, ['FATAL. %s:' % (message), traceback.format_exc().replace('\n', '__')])

