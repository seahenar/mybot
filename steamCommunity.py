import urllib, urllib2, cookielib, datetime, time, feedparser, random
import json as simplejson
from M2Crypto import RSA, m2

class SteamCommunity:
	"""Add events and announcements to a steam community group"""
	rsaUrl		= 'https://steamcommunity.com/login/getrsakey/'
	loginurl	= 'https://steamcommunity.com/login/dologin/'
	announceurl	= 'http://steamcommunity.com/groups/%s/announcements'
	eventurl	= 'http://steamcommunity.com/groups/%s/eventEdit'
	rssurl		= 'http://steamcommunity.com/groups/%s/rss'
	deleteurl	= 'http://steamcommunity.com/groups/%s/eventEdit?action=deleteEvent&eventID=%s'
	types 		= [
		'ChatEvent', 'OtherEvent', 'PartyEvent', 'MeetingEvent',
		'SpecialCauseEvent', 'MusicAndArtsEvent', 'SportsEvent',
		'TripEvent', 'GameEvent'
	]

	def __init__(self,username,password):
		"""Prepare url handler and cookie jar so that the cookies are unique to the instance of the oject
			to remove the need to login every time"""
		self.debug			= True
		self.username		= username
		self.password		= password
		self.cookies		= cookielib.CookieJar()
		self.cookieHandler	= urllib2.HTTPCookieProcessor(self.cookies)
		self.redirectionHandler = urllib2.HTTPRedirectHandler()
		self.opener	= urllib2.build_opener(self.cookieHandler, self.redirectionHandler)
		self.reqHandle = None
		urllib2.install_opener(self.opener)
		self.setOffsetCookie()

	def utcOffset(self):
		"""Return current UTC offset in seconds"""
		isdst = time.localtime().tm_isdst
		if (time.daylight and isdst):
			return -time.altzone
		return -time.timezone

	def setOffsetCookie(self):
		"""Set the timezoneOffset cookie, without this all event times will be set to GMT+8"""
		self.cookies.set_cookie(cookielib.Cookie(version=0, name='timezoneOffset', value="%s,0" % self.utcOffset(),
			port=None, port_specified=False, domain='steamcommunity.com',
			domain_specified=False, domain_initial_dot=False, path='/',
			path_specified=True, secure=False, expires=None, discard=True,
			comment=None, comment_url=None, rest={'HttpOnly': None}, rfc2109=False))
		return True
		

	def login(self):
		"""Perform Login post and return SteamAuth cookie to perform other fuctions"""
		data = urllib.urlencode({
			'username': self.username,
			'donotcache': self.unix_time()
		})

		if self.debug:
			print 'begin get RSA key attempt to', self.rsaUrl, data

		req = urllib2.urlopen(self.rsaUrl, data)
		response = simplejson.load(req)
		req.close()
		if self.debug:
			print 'rsa key', response

		# convert to openSSH key format
		bnE=m2.hex_to_bn(response['publickey_exp'])
		bnN=m2.hex_to_bn(response['publickey_mod'])
		exp=m2.bn_to_mpi(bnE)
		mod=m2.bn_to_mpi(bnN)

		pubKey = RSA.new_pub_key((exp, mod))
		encryptedPassword = pubKey.public_encrypt(self.password, RSA.pkcs1_padding).encode('base64').replace('\n', '')

		if self.debug:
			print 'length and encrypted password', len(encryptedPassword), encryptedPassword

		data = urllib.urlencode({
			'password': encryptedPassword,
			'username': self.username,
			'emailauth': '',
			'captchagid': '-1',
			'captcha_text': '',
			'emailsteamid': '',
			'rsatimestamp': response['timestamp'],
			'donotcache': self.unix_time()
		})

		if self.debug:
			print 'begin login attempt to:', self.loginurl, data

		loginRequest = urllib2.Request(self.loginurl, data)
		loginRequest.add_header('Referer', 'https://steamcommunity.com/login/home')
		loginRequest.add_header('Host', 'steamcommunity.com')
		loginRequest.add_header('Accept', 'application/json')
		loginRequest.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0')
		self.reqHandle = urllib2.urlopen(loginRequest)

		if self.debug:
			print 'login response:', self.reqHandle.read()

		if self.debug:
			print 'login request completed', self.reqHandle.getcode()

		return self.hasSteamCookie()

	def hasSteamCookie(self):
		"""Check that the steamLogin cookie is in the cookiejar, to prove we are logged in"""
		for cookie in self.cookies:
			if self.debug:
				print 'got cookie', cookie.name

			if cookie.name == 'steamLogin':
				if self.debug:
					print 'got steam cookie'
				return True

		if self.debug:
			print 'could not find steam cookie'

		return False

	def announce(self, group, headline, body):
		"""Login if neccessary and then submit announcement
			group -- Name of the group
			headline -- Headline string of the announcement
			body -- Full text of the ammouncement
		"""
		if (self.hasSteamCookie()):
			self.announcePost(group, headline, body)
			if (self.hasSteamCookie()):
				return True
			
		if not self.login():
			return False

		self.announcePost(group, headline, body)
		
		if self.hasSteamCookie():
			return True
		return False

	def announcePost(self, group, headline, body):
		"""Submit announcement
			This is called by announce to post the form.
			Takes the same arguments as announce.
			Should not be called directly.
		"""
		data = urllib.urlencode({
			'action': 'post',
			'headline': headline,
			'body': body,
		})
		self.reqHandle = urllib2.urlopen(self.announceurl % group, data)
		return self.hasSteamCookie()
	
	def eventSpecific(self, group, name, type, notes, eventDate, appID=None, serverIP=None, serverPassword=None):
		"""Login if neccessary and then submit event
			group -- Name of the group
			name -- Headline string of the event
			type -- A valid event type, as listed in self.types
			notes -- Full text of the ammouncement
			eventDate -- datetime of the event
			appID -- steam appid for the game, if type gameevent (Default: None)
			serverIP -- ip:port for the arranged game if type gameevent (Default: None)
			serverPassword -- password for the above server, only visable by members of group
				(Default: None)
		"""
		key = '##KEY=%s##' % hex(random.getrandbits(64))
		if (not self.hasSteamCookie()):
			if not self.login():
				return False

		if self.debug:
			print 'logged in, begin event creation'

		self.eventSpecificPost(group, 'Upcoming Event', 'ChatEvent', key, datetime.datetime.now());
		
		if not self.hasSteamCookie():
			return False

		rss = feedparser.parse(self.rssurl % group)
		eid = False
		for entry in rss.entries:
			if entry.description == "Chat: %s" % key:
				eid = entry.link.split('/')[-1]
			elif entry.description[0:12] == "Chat: ##KEY=":
				'''Clean up any outstanding ones that must have failed'''
				urllib2.urlopen(self.deleteurl % (group, entry.link.split('/')[-1]))
		if eid:
			if self.eventSpecificModify(group, eid, name, type, notes, eventDate, appID, serverIP, serverPassword):
				return eid
		return False


	def eventSpecificModify(self, group, eid, name, type, notes, eventDate, appID=None, serverIP=None, serverPassword=None):
		"""Login if neccessary and then modify event
			group -- Name of the group
			eid -- eventID set by steamcommunity.com to uniquely identify the event
			name -- Headline string of the event
			type -- A valid event type, as listed in self.types
			notes -- Full text of the ammouncement
			eventDate -- datetime of the event
			appID -- steam appid for the game, if type gameevent (Default: None)
			serverIP -- ip:port for the arranged game if type gameevent (Default: None)
			serverPassword -- password for the above server, only visable by members of group
				(Default: None)
		"""
		if (not self.hasSteamCookie()):
			if not self.login():
				return False

		self.eventSpecificModifyPost(group, eid, name, type, notes, eventDate, appID, serverIP, serverPassword)
		
		if not self.hasSteamCookie():
			return False
		return True

	def eventSpecificPost(self, group, name, type, notes, eventDate, appID=None, serverIP=None, serverPassword=None ):
		"""Submit Event
			This is called by eventSpecific to post the form.
			Takes the same arguments as eventSpecific.
			Should not be called directly.
		"""
		if not type in self.types:
			return False
		if 'GameEvent' == type and None == appID:
			return False
		self.setOffsetCookie()
		(startDate, startHour, startMinute, startAMPM) = eventDate.strftime('%m/%d/%y %I %M %p').split(' ')
		data = urllib.urlencode({
	 		'action': 'newEvent',
			'name': name,
			'type': type,
			'notes': notes,
			'tzOffset': self.utcOffset,
			'eventQuickTime': 'now',
			'timeChoice': 'specific',
			'appID': appID,
			'serverIP': serverIP,
			'serverPassword': serverPassword,
			'startDate': startDate,
			'startHour': startHour,
			'startMinute': startMinute,
			'startAMPM': startAMPM
		})
		self.reqHandle = urllib2.urlopen(self.eventurl % group, data)
		return self.hasSteamCookie()

	def eventSpecificModifyPost(self, group, eid, name, type, notes, eventDate, appID=None, serverIP=None, serverPassword=None ):
		"""Submit Event
			This is called by eventSpecificModify to post the form.
			Takes the same arguments as eventSpecificModify.
			Should not be called directly.
		"""
		if not type in self.types:
			return False
		if 'GameEvent' == type and None == appID:
			return False
		self.setOffsetCookie()
		(startDate, startHour, startMinute, startAMPM) = eventDate.strftime('%m/%d/%y %I %M %p').split(' ')
		data = urllib.urlencode({
	 		'action': 'updateEvent',
	 		'eventID': eid,
			'name': name,
			'type': type,
			'notes': notes,
			'tzOffset': self.utcOffset,
			'eventQuickTime': 'now',
			'timeChoice': 'specific',
			'appID': appID,
			'serverIP': serverIP,
			'serverPassword': serverPassword,
			'startDate': startDate,
			'startHour': startHour,
			'startMinute': startMinute,
			'startAMPM': startAMPM
		})
		self.reqHandle = urllib2.urlopen(self.eventurl % group, data)
		return self.hasSteamCookie()

	def eventDelete(self, group, eid):
		"""Delete Event
			This is called to attempt a delete of an event, specified by eid.

			group -- Name of the group
			eid -- eventID set by steamcommunity.com to uniquely identify the event
		"""
		if (self.hasSteamCookie()):
			self.announcePost(group, headline, body)
			if (self.hasSteamCookie()):
				return True
			
		if not self.login():
			return False

		urllib2.urlopen(self.deleteurl % (group, eid))
		
		if self.hasSteamCookie():
			return True

		return False

	def unix_time(self):
		epoch = datetime.datetime.utcfromtimestamp(0)
		delta = datetime.datetime.now() - epoch
		return int(delta.total_seconds()) * 1000

#"""Example login"""
#steam = SteamCommunity('cogsbot', 'X5f2VjX0')
#eid = steam.eventSpecific('cogsbot', 'Another Event', 'PartyEvent', '5', datetime.datetime.fromtimestamp(time.time() + 86400 * 1 ))
#print eid

#steam.eventDelete('homgtest',1027014441513953076)
