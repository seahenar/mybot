import irc

class Message:

	def __init__(self, sender, channel, text):
		self.sender = sender
		self.destination = channel
		self.text = text
	
		args = text.strip().split()
		if len(args) == 0: # some commands don't have a message
			self.command = ''
			self.args = []
		else:
			self.command = args[0].lower()
			self.args = filter(lambda arg:arg != '', args[1:])

		self.numArgs = len(self.args)

	def isBotCommand(self):
		return self.text.startswith('!')

	def isChannelDestination(self):
		return self.destination.startswith('#')

	def joinFrom(self, index, joiner = ' '):
		return joiner.join(self.args[index:])

	def reply(self, response):
		if isinstance(response, str) or isinstance(response, unicode):
			response = [response]

		irc.sendMultilineMsg(self.sender, response)
