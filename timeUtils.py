import re
from datetime import datetime, timedelta

delayRegex  = re.compile('((?P<h>\d{,2})h)?((?P<m>\d{,2})m)?$', re.I)
militaryRegex = re.compile('(\d{4})$', re.I)
timeRegex     = re.compile('(\d{1,2})[\.:](\d{1,2})(am|pm)?$')
dateRegex     = re.compile('(\d{,2})[/\.-](\d{,2})[/\.-](\d{2,4})$')

timeFormat    = '%02d/%02d/%d %02d:%02d'

class DateParser:
	def __init__(self, value = None, error = None, argsUsed = 2):
		self.value = value
		self.error = error
		self.argsUsed = argsUsed 
		self.success = True if value is not None else False

def getDateTime(dateOrTime, time):
	dateResult = getDateInFuture(dateOrTime)
	if not isinstance(dateResult, datetime):
		timeResult = getTimeInFuture(dateOrTime)
		if not isinstance(timeResult, datetime):
			return DateParser(error = dateResult + ' ' + timeResult)
		return DateParser(value = timeResult, argsUsed = 1)

	timeResult = getTimeInFuture(time)
	if not isinstance(timeResult, datetime):
		return DateParser(error = timeResult)

	matchTime = merge(dateResult, timeResult)
	if not isinstance(matchTime, datetime):
		return DateParser(error = matchTime)

	return DateParser(value = matchTime)

def getTimeInFuture(input):
	if input == '':
		return 'You haven\'t specified a time.'

	regexMatch = timeRegex.match(input)
	if regexMatch != None:
		hour = int(regexMatch.group(1))
		minute = int(regexMatch.group(2))
		meridian = regexMatch.group(3)

		if meridian:
			if hour < 1 or hour > 12:
				return 'You cannot specify am/pm with an hour value of ' + str(hour)
			if meridian == 'pm':
				hour += 12
		return getTime(hour, minute)
	
	regexMatch = militaryRegex.match(input)
	if regexMatch != None:
		military = regexMatch.groups(1)[0]
		hour = int(military[0:2])
		minute = int(military[2:4])

		return getTime(hour, minute)

	regexMatch = delayRegex.match(input)
	if regexMatch != None:
		hours   = regexMatch.group('h')
		minutes = regexMatch.group('m')
		hours   = 0 if hours == None else int(hours)
		minutes = 0 if minutes == None else int(minutes)

		return getReturnTime(hours, minutes)
	else:
		return 'I can\'t parse the time you entered. You can specify time in many ways: either 24h/am/pm/military time, or as a delay (e.g. 1h45m)'

def getDateInFuture(input):
	regexMatch = dateRegex.match(input)
	if regexMatch != None:
		day = int(regexMatch.group(1))
		month = int(regexMatch.group(2))
		year = regexMatch.group(3)
		if len(year) == 2:
			year = '20' + year

		result = datetime(int(year), int(month), int(day))
		now = datetime.now()
		if result.year > (now.year + 9000):
			return 'You can\'t specify years more than 9000 years in the future.'
		now = datetime(now.year, now.month, now.day)
		if result < now:
			return 'You can\'t specify a date in the past.'
		return result
	else:
		return 'I can\'t parse the date you entered. Try dd/mm/yy.'

def merge(date, time):
	result = datetime(date.year, date.month, date.day, time.hour, time.minute)
	now = datetime.now()
	if result < now:
		return 'Your date & time combination is in the past! Please check your values'
	return result

def getTime(hour, minute):
	if hour > 23:
		return 'You cannot specify an hour greater than 23'
	if minute > 59:
		return 'You cannot specify a minute greater than 59'

	now = datetime.now()
	remindTime = now
	if hour < now.hour or hour == now.hour and minute <= now.minute:
		remindTime += timedelta(days = 1)
	return datetime(remindTime.year, remindTime.month, remindTime.day, hour, minute)

def getReturnTime(hours, minutes):
	curTime = datetime.now()
	waitTime = timedelta(hours=hours, minutes=minutes)
	return curTime + waitTime

def toDatabase(time):
	return time.isoformat()

def fromDatabase(string):
	parts = string.split('T')
	date = parts[0].split('-')
	time = parts[1].split(':')
	if '.' in time[2]:
		secs = time[2].split('.')
	else:
		secs = (time[2], 0)
	return datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(secs[0]), int(secs[1]))

def getFriendlyTime(time):
	return timeFormat % (time.day, time.month, time.year, time.hour, time.minute)

def fromFriendlyTime(time):
	parts = time.split()
	time = parts[0]
	date = parts[1]
	times = time.split(':')
	dates = date.split('/')
	return datetime(hour=int(times[0]), minute=int(times[1]), day=int(dates[0]), month=int(dates[1]), year=int(dates[2]))

def getDelta(diff):
	if diff.days >= 1:
		if diff.days == 1:
			return '1 day ago'
		else:
			return str(diff.days) + ' days ago'

	elif diff.seconds <= 3600:
		if diff.seconds <= 30:
			return 'seconds ago'
		elif diff.seconds <= 90:
			return '1 minute ago'
		else:
			return str(diff.seconds / 60) + ' minutes ago'

	elif diff.seconds <= 86400:
		if diff.seconds <= 5400:
			return '1 hour ago'
		else:
			return str(diff.seconds / 3600) + ' hours ago'

	else:
		return 'seconds ago'

def getNextWorkingDay(date = None):
	if date == None:
		date = datetime.now()
	untilNextWorkingDay = 1
	print 'using date', date
	dayOfWeek = date.weekday()
	print 'dayofweek is', dayOfWeek

	if dayOfWeek == 4:
		untilNextWorkingDay = 3
	if dayOfWeek == 5:
		untilNextWorkingDay = 2
	
	timeDelta = timedelta(days = untilNextWorkingDay)
	nextDay = date + timeDelta
	return nextDay

def isWorkingDay(date = None):
	if date == None:
		date = datetime.now()
	return date.weekday() != 5 and date.weekday() != 6

def isToday(date, today = None):
	if today == None:
		today = datetime.now()
	return date.day == today.day and date.month == today.month and date.year == today.year

def isTomorrow(date):
	return isToday(date, datetime.now() + timedelta(days = 1))

def dayDifference(date):
	if isToday(date):
		return 'today'
	elif isTomorrow(date):
		return 'tomorrow'
	else:
		return 'on ' + date.strftime('%A')
