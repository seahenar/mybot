import re

def isListMatch(value, text):
	value = re.escape(value)
	pattern = re.compile('(^| )' + value + ' | ' + value + '$', re.I)
	return pattern.search(text) is not None

def removeFromList(value, text):
	value = re.escape(value)
	pattern = re.compile(' ' + value + ' ', re.I)
	if pattern.search(text) is None:
		pattern = re.compile('^' + value + ' | ' + value + '$', re.I)
		return pattern.sub('', text)
	else:
		return pattern.sub(' ', text)

def removeColourCodes(text):
	return colourCode.sub('', text)

dangerPattern = re.compile('4chan.org/([bdehstuy]|gif|hc|hr|r9k)/|apina.biz|redtube.com|porn.com')
nsfwPattern = re.compile('(^| |\\()(n)?sfw( |$|\\)|,|\\.)', re.I)
colourCode = re.compile(u'\u0003\d{0,2}')
def isDangerMessage(message):
	return ('http://' in message or 'www.' in message) and nsfwPattern.search(message) is None and dangerPattern.search(message) is not None
