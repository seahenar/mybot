import config
import database
import timeUtils
import userTracker
from handlerBase import HandlerBase, Command
from datetime import datetime
import handler

class Tell:
	def __init__(self, sender, date, recipient, message):
		self.sender = sender
		self.date = date
		self.recipient = recipient
		self.message = message

	def getTellMessageAndRemoveFromDb(self):
		database.execute('delete from tell where sender=? and dateAdded=? and recipient=? and message=?', (self.sender, self.date, self.recipient, self.message))
		return self.sender + ' left you a message ' + timeUtils.getDelta(datetime.now() - self.date) + u': \u0002' + self.message

class TellHandler(HandlerBase):

	def __init__(self, levels):
		self.usage = '!tell <user> <message>'
		self.help = u'Use \u0002!tell\u0002 to leave a message for somebody who is offline. As soon as they re-appear online, your message will be relayed to them, along with the time you left it for them'
		HandlerBase.__init__(self, levels, Command('!tell', self.handle, self.help, self.usage))
		handler.registerGlobalHandler('tell', self)

		#impose limit somehow? on sender or on recipient?
		self.maxTells = 5
		self.tells = {}

		storedTells = database.execute('select sender, dateAdded as "dateAdded [timestamp]", recipient, message from tell', '').fetchall()
		for tell in storedTells:
			self.addTell(tell['sender'], tell['dateAdded'], tell['recipient'], tell['message'])

	def handle(self, message):
		if message.numArgs >= 2:
			sender = message.sender
			time = datetime.now()
			recipient = message.args[0]
			message = ' '.join(message.args[1:])

			if not self.isValidNickname(recipient):
				self.sendMsg(sender, '\'' + recipient + '\' is not a valid nickname.')
				return

			if not userTracker.isOnline(recipient):
				if not config.enabled('userPrefs') or config.enabled('userPrefs') and self.getHandler('userPrefs').getSetting('tell', recipient) != 'off':
					self.addTell(sender, time, recipient, message)
					database.execute('insert into tell values (?, ?, ?, ?)', (sender, time, recipient, message))
					self.sendMsg(sender, 'Your message will be delivered next time ' + recipient + ' comes online.')
				else:
					self.sendMsg(sender, 'Sorry, ' + recipient + ' has opted not to receive tells -- you cannot leave messages for them unless they renable this setting')
			else:
				self.sendMsg(sender, recipient + ' is online now. You can send them a private message instead!')
		else:
			msgs = [self.help]
			msgs.append('usage: ' + self.usage)
			self.sendMultilineMsg(message.sender, msgs)

	def addTell(self, sender, time, recipient, message):
		tell = Tell(sender, time, recipient, message)
		recipient = recipient.lower()
		if recipient in self.tells.keys():
			self.tells[recipient].append(tell)
		else:
			self.tells[recipient] = [tell]

	def userJoined(self, origUser):
		user = origUser.lower()
		if user in self.tells.keys():
			for tell in self.tells[user]:
				self.sendMsg(origUser, tell.getTellMessageAndRemoveFromDb(), msgType = 'tell')
			del self.tells[user]

def __init__(levels):
	TellHandler(levels)
