import config
import database
import userTracker
from handlerBase import HandlerBase, Command
import handler
from datetime import datetime, timedelta
import regex

class ListHandler(HandlerBase):

	def __init__(self, levels):
		self.updlistUsage = '!updlist <listname> <action> <value>'

		if 'listadd' in levels:
			HandlerBase.__init__(self, levels['listadd'], Command('!listadd', self.handleListAdd))
		if 'listdel' in levels:
			HandlerBase.__init__(self, levels['listdel'], Command('!listdel', self.handleListDel))
		if 'gamelistadd' in levels:
			HandlerBase.__init__(self, levels['gamelistadd'], Command('!gamelistadd', self.handleGameListAdd))
		if 'gamelistdel' in levels:
			HandlerBase.__init__(self, levels['gamelistdel'], Command('!gamelistdel', self.handleGameListDel))
		if 'maintainerupdlist' in levels:
			HandlerBase.__init__(self, levels['maintainerupdlist'], Command('!updlist', self.handleMaintainerUpdateList, 'If you are added as a maintainer for a list, you will be able to use this to modify lists you have control over', self.updlistUsage))
		
		if 'adminupdlist' in levels:
			HandlerBase.__init__(self, levels['adminupdlist'], Command('!updlist', self.handleAdminUpdateList))

		handler.registerGlobalHandler('lists', self)

	def handleListAdd(self, message):
		if message.numArgs >= 2:
			self.createList(message.sender, message.args[0].lower(), ' '.join(message.args[1:]), False)
		else:
			self.sendMsg(message.sender, 'usage: !listadd <listName> <description>')

	def handleGameListAdd(self, message):
		if message.numArgs >= 2:
			self.createList(message.sender, message.args[0].lower(), ' '.join(message.args[1:]), True)
		else:
			self.sendMsg(message.sender, 'usage: !gamelistadd <listName> <description>')

	def createList(self, user, listName, description, isGameList):
		if self.isInvalidListName(listName):
			self.sendMsg(user, 'List names cannot end in any of add, del, list')
			return
		if not self.listExists(listName):
			database.execute("insert into lists values (?, '', ?)", (listName,description))
			if isGameList:
				database.execute('insert into gamelists values (?)', (listName,))

			self.sendMsg(user, 'List \'!' + listName + '\' added')
			message = u'A new list has been added. \u0002!' + listName + u'\u0002 to view'
			self.inform(config.channel, message)
			self.informPeopleOfNewList(message, isGameList)
		else:
			self.sendMsg(user, 'The list \'' + listName + '\' already exists')

	def informPeopleOfNewList(self, message, isGameList):
		people = self.getHandler('userPrefs').getPeopleWhoWantInformingAboutLists()

		for person in people.keys():
			if people[person] == 'all' or people[person] == 'games' and isGameList or people[person] == 'events' and not isGameList:
				self.sendMsg(person, message, evenIfOffline = True)

	def handleListDel(self, message):
		if message.numArgs == 1:
			self.deleteList(message.sender, message.args[0])
		else:
			self.sendMsg(message.sender, 'usage: !listdel <listName>')

	def handleGameListDel(self, message):
		if message.numArgs == 1:
			self.deleteList(message.sender, message.args[0])
		else:
			self.sendMsg(message.sender, 'usage: !gamelistdel <listName>')

	def deleteList(self, user, listName):
		if self.listExists(listName):
			database.execute('delete from lists where key=?', (listName,))
		else:
			self.sendMsg(user, 'The list \'' + listName + '\' does not exist')
			return
		if self.isGameList(listName):
			database.execute('delete from gamelists where key=?', (listName,))
		self.sendMsg(user, 'List \'!' + listName + '\' deleted')

	def addToList(self, key, value):
		curValue = self.getList(key)[0]
		newValue = value

		if curValue is not '':
			if not regex.isListMatch(value, curValue):
				newValue = curValue + ' ' + value
			else:
				return "you are already in the list!"
		database.execute("update lists set value=? where key=?", (newValue, key))

	def delFromList(self, key, value):
		curValue = self.getList(key)[0]
		newValue = regex.removeFromList(value, curValue)
		database.execute("update lists set value=? where key=?", (newValue, key))

	def getListMaintainers(self, list):
		users = database.execute('select user from listmaintainers where list = ?', (list.lower(),)).fetchall()
		if len(users) > 0:
			return [u['user'] for u in users]
		return []

	def getListsUserCanMaintain(self, user):
		users = database.execute('select list from listmaintainers where user = ?', (user.lower(),)).fetchall()
		if len(users) > 0:
			return [u['list'] for u in users]
		return []

	def addListMaintainer(self, sender, list, user):
		if user.lower() in self.getListMaintainers(list):
			self.sendMsg(sender, '%s is already a maintainer of the %s list' % (user, list))
		else:
			database.execute('insert into listmaintainers values (?, ?)', (user.lower(), list.lower()))
			self.sendMsg(sender, 'List maintainer added successfully')

	def removeListMaintainer(self, sender, list, user):
		if user.lower() in self.getListMaintainers(list):
			database.execute('delete from listmaintainers where user = ? and list = ?', (user.lower(), list.lower()))
			self.sendMsg(sender, 'List maintainer removed successfully')
		else:
			self.sendMsg(sender, '%s is not a maintainer of the %s list. Nothing to do.' % (user, list))

	def listExists(self, listName):
		result = database.execute('select key from lists where key=?', (listName,))
		return result.fetchone() != None

	def getList(self, listName):
		result = database.execute('select value, description from lists where key=?', (listName,))
		return result.fetchone()

	def getLists(self):
		return self.getListsOfType('lists')

	def isInvalidListName(self, listName):
		return listName.endswith('add') or listName.endswith('del') or listName.endswith('list')

	def getListsOfType(self, type):
		result = database.execute('select key from ' + type, '')
		retVal = []
		results = result.fetchall()
		results.sort()
		for res in results:
			retVal.append('!' + res[0])

		return retVal

	# game list stuff
	usage = {}
	triggerInterval = 5

	def isGameList(self, key):
		result = database.execute('select key from gamelists where key=?', (key,))
		return result.fetchone() != None

	def getGameLists(self):
		return self.getListsOfType('gamelists')

	def canHighlight(self, key):
		if key not in self.usage:
			self.usage[key] = datetime.now()
			return True
		else:
			lastRun = self.usage[key]
			curTime = datetime.now()
			diff = curTime - lastRun
			waitTime = timedelta(minutes=self.triggerInterval)
			if diff > waitTime:
				self.usage[key] = datetime.now()
				return True
			else:
				wait = waitTime - diff
				minutes = wait.seconds / 60
				seconds = wait.seconds - (60 * minutes)
				return 'Games can only be triggered every ' + str(self.triggerInterval) + ' minutes. You must wait ' + str(minutes) + 'm' + str(seconds) + 'sec.'

	def handleMaintainerUpdateList(self, message):
		lists = self.getListsUserCanMaintain(message.sender)
		if len(lists) == 0:
			self.sendMsg(message.sender, 'You are not a maintainer of any lists')
			return

		if not self.isAuthedUser(message.sender):
			self.sendMsg(message.sender, 'You must be identified with nickserv to use this command')
			return

		if message.numArgs <= 2:
			self.sendMsg(message.sender, 'You are a maintainer of the following lists: %s' % ', '.join(lists))
		self.handleUpdateList(message, lists)

	def handleAdminUpdateList(self, message):
		lists = [item['key'] for item in database.execute('select key from lists', '').fetchall()]
		self.handleUpdateList(message, lists)

	# updating lists
	updListHelp = {
	'descr': u'Changes the description of the list. E.g. \u0002!updlist st description lololol\u0002 would set the description of the \u0002!st\u0002 list to be \'lololol\'',
	'add': u'Adds someone to a list. E.g. \u0002!updlist st add fuckface\u0002 would add \'fuckface\' to the \u0002!st\u0002 list',
	'del': u'Removes someone from a list. E.g. \u0002!updlist st del fuckface\u0002 would remove \'fuckface\' from the \u0002!st\u0002 list',
	'maintaineradd': 'Add a maintainer for this list',
	'maintainerdel': 'Remove a maintainer for this list'
	}
	def handleUpdateList(self, message, availableLists):
		if message.numArgs >= 1:
			if message.args[0].lower() in availableLists:
				if message.numArgs >= 2:
					if message.args[1] in self.updListHelp.keys():
						if message.numArgs >= 3:
							if message.args[1] == 'descr':
								self.updateDescription(message.sender, message.args[0], ' '.join(message.args[2:]))
							elif message.args[1] == 'add':
								self.addToList(message.args[0], message.args[2])
								self.sendMsg(message.sender, 'User added to list successfully')
							elif message.args[1] == 'del':
								self.delFromList(message.args[0], message.args[2])
								self.sendMsg(message.sender, 'User removed from list successfully')
							elif message.args[1] == 'maintaineradd':
								self.addListMaintainer(message.sender, message.args[0], message.args[2])
							elif message.args[1] == 'maintainerdel':
								self.removeListMaintainer(message.sender, message.args[0], message.args[2])
						else:
							self.sendMsg(message.sender, self.updListHelp[message.args[1]])
					else:
						self.sendMsg(message.sender, '\'' + message.args[1] + '\' is not a recognised action')
						self.sendMsg(message.sender, 'The available actions are: ' + ', '.join(self.updListHelp.keys()))
				else:
					self.sendMsg(message.sender, 'The list \'' + message.args[0] + '\' exists. You must specify an action to perform upon it')
					actions = self.updListHelp.keys()
					actions.sort()
					self.sendMsg(message.sender, 'The available actions are: ' + ', '.join(actions))
					self.sendMsg(message.sender, 'usage: ' + self.updlistUsage)
			else:
				self.sendMsg(message.sender, 'The list \'' + message.args[0] + '\' does not exist')
				self.sendMsg(message.sender, 'usage: ' + self.updlistUsage)
		else:
			self.sendMsg(message.sender, 'usage: ' + self.updlistUsage)
			self.sendMsg(message.sender, 'If you need assistance using this command, you can supply the arguments in stages for contextual help')
	
	def updateDescription(self, sender, key, value):
		database.execute("update lists set description=? where key=?", (value, key))
		self.sendMsg(sender, 'List description updated successfully')

	# user list interaction
	def handleList(self, message):
		cmd = message.command
		sender = message.sender

		cmd = cmd.lstrip('!')
		rawcmd = cmd
		if cmd.endswith('add') or cmd.endswith('del'):
			if len(cmd) > 3:
				rawcmd = cmd[:-3]
		if cmd.endswith('list'):
			if len(cmd) > 4:
				rawcmd = cmd[:-4]

		if self.listExists(rawcmd):
			if cmd.endswith('add'):
				#if self.isAuthedUser(sender):
				self.addToList(rawcmd, sender)
				self.sendMsg(sender, u'You have been added to the \'\u0002!' + rawcmd + u'\'\u0002 list')
				#else:
				#	self.sendRegistrationExplanation(sender)
			elif cmd.endswith('del'):
				#if self.isAuthedUser(sender):
				self.delFromList(rawcmd, sender)
				self.sendMsg(sender, u'You have been removed from the \'\u0002!' + rawcmd + u'\'\u0002 list')
				#else:
				#	self.sendRegistrationExplanation(sender)
			elif cmd.endswith('list'):
				list = self.getList(rawcmd)
				isGameList = self.isGameList(rawcmd)
				self.sendListMembersToUser(rawcmd, message, list[1], list[0], isGameList)
			else:
				list = self.getList(rawcmd)
				if self.isGameList(rawcmd):
					if regex.isListMatch(sender, list[0]):
						canTrigger = self.canHighlight(rawcmd)
						if canTrigger == True:
							people = list[0].strip().split()
							people = userTracker.removeOfflineUsers(people)
							people = [person for person in people if person.lower() != sender.lower()]
							i = 0
							numPeople = len(people)
							while i < numPeople:
								person = people[i]
								if 'away' in config.handlers and self.getHandler('away').isAway(person):
									people.remove(person)
									numPeople = len(people)
								i += 1
							peopleStr = ' '.join(people)
							self.sendMsg(sender, 'Alerting ' + peopleStr + ' that you want to play ' + rawcmd)
							self.inform(config.gamingChannel, sender + ' wants to play ' + rawcmd + '!')
							for person in people:
								self.sendMsg(person, sender + ' wants to play ' + rawcmd + '!', 'games')
						else:
							self.sendMsg(sender, canTrigger)
					else:
						self.sendMsg(sender, u'You must be a member of this game list to tell others you want to play it. \u0002!' + rawcmd + u'add\u0002 to join.')
						self.sendMsg(sender, '\'!' + rawcmd + '\' players (' + str(len(list[0].split())) + '): ' + list[0])
				else:
					self.sendListMembersToUser(rawcmd, message, list[1], list[0], False)

	def sendListMembersToUser(self, listName, message, description, members, isGameList):
		memberList = members.split()
		if message.numArgs >= 1:
			attendeeToQuery = message.args[0]
			isOrIsnt = 'is' if attendeeToQuery.lower() in [m.lower() for m in memberList] else 'is not'
			message.reply(attendeeToQuery + u' \u0002' + isOrIsnt + u'\u0002 on the !' + listName +  ' list')
			return

		msgs = []
		msgs.append(description)
		maintainers = self.getListMaintainers(listName)
		if len(maintainers) > 0:
			msgs.append('List maintainers: ' + ', '.join(maintainers))
		type = '\' attendees (' if not isGameList else '\' players ('
		msgs.append('\'!' + listName + type + str(len(memberList)) + '): ' + members)

		self.sendMultilineMsg(message.sender, msgs)

def __init__(levels):
	ListHandler(levels)
