from handlerBase import HandlerBase, Command
import constants
import database
import scheduler
import timeUtils
from datetime import datetime

class RemindHandler(HandlerBase):

	def __init__(self, levels):

		self.remindUsage = '!remind [date] <time> <message>'
		self.remindersUsage = '!reminders <action> [number]'
		HandlerBase.__init__(self, levels, Command('!remind', self.handleRemind, 'Sets a reminder for a certain event after a period of time or on a particular day/time. Useful if you don\'t want to forget something (like your dinner in the oven) whilst you\'re at the computer', self.remindUsage))
		HandlerBase.__init__(self, levels, Command('!reminders', self.handleReminders, u'Use this command to manage your current list of pending reminders. You can delete a specific reminder, or clear all of them. Type \u0002!reminders\u0002 for a list of your current reminders, and help administering them', self.remindersUsage))
		self.persistentType = 'remind'
		self.maxReminders = 5
		self.reminders = {}

		reminderData = database.retrievePersistentStore(self.persistentType)
		for item in reminderData:
			user = item[0].lower()
			parts = item[1].split()
			timeDue = timeUtils.fromDatabase(parts[0])
			reminder = ' '.join(parts[1:])
			if timeDue >= datetime.now():
				self.addReminder(user, timeDue, reminder, True)
			else:
				database.deleteFromPersistentStore(self.persistentType, user, parts[0] + ' ' + reminder)

	def handleRemind(self, message):
		if message.numArgs >= 2:
			gotDate = False
			time = None

			if message.numArgs >= 3:
				date = self.tryParseDate(message.sender, message.args[0])
				if isinstance(date, datetime):
					gotDate = True
					time = self.tryParseTime(message.sender, message.args[1])
					if not isinstance(time, datetime):
						self.sendMsg(message.sender, time)
						return
				elif date != 'I can\'t parse the date you entered. Try dd/mm/yy.' and date != None:
					self.sendMsg(message.sender, date)
					return
			
			if time == None:
				time = self.tryParseTime(message.sender, message.args[0])

			if isinstance(time, datetime):
				if gotDate:
					result = timeUtils.merge(date, time)
					if not isinstance(result, datetime):
						self.sendMsg(message.sender, result)
						return
					reminder = ' '.join(message.args[2:])
				else:
					result = time
					reminder = ' '.join(message.args[1:])

				added = self.addReminder(message.sender.lower(), result, reminder)
				if added == False:
					self.sendMsg(message.sender, constants.getConstant('tooManyReminds') % self.maxReminders)
				else:
					self.sendMsg(message.sender, 'Reminder \'' + reminder + '\' added. You will be reminded on %d/%d/%d at %02d:%02d' % (result.day, result.month, result.year, result.hour, result.minute))
					self.sendMsg(message.sender, u'Use \u0002!reminders\u0002 to review/edit your current reminders')
			else:
				self.sendMsg(message.sender, time)
		else:
			self.sendMsg(message.sender, 'usage: ' + self.remindUsage)
			self.sendMsg(message.sender, constants.getConstant('remindInfo'))

	remindersHelp = {
		'clear': 'clear all of your reminders',
		'del': 'Use the del action to remove a numbered reminder from your list of reminders'
	}
	def handleReminders(self, message):
		reminders = database.retrieveOrderedFromPersistentStore(self.persistentType, message.sender.lower())
		if message.numArgs == 2:
			if message.args[0] == 'del':
				if self.isValidIndex(message.args[1], reminders):
					index = int(message.args[1]) - 1
					database.deleteFromPersistentStore(self.persistentType, message.sender.lower(), reminders[index][0])
					self.removeRemindersFromScheduler([reminders[index]], message.sender.lower())
					self.sendMsg(message.sender, 'Reminder deleted')
				else:
					self.sendMsg(message.sender, 'Reminders unchanged. The number must correspond to a reminder number')
		elif message.numArgs == 1:
			if message.args[0] == 'clear':
				if len(reminders) > 0:
					database.deleteManyFromPersistentStore(self.persistentType, message.sender.lower())
					self.removeRemindersFromScheduler(reminders, message.sender.lower())
					self.sendMsg(message.sender, 'All of your reminders have been cleared')
				else:
					self.sendMsg(message.sender, 'You don\'t have any reminders to clear!')
			elif message.args[0] in self.remindersHelp:
				self.sendMsg(message.sender, 'usage: ' + self.remindersUsage)
				self.sendMsg(message.sender, self.remindersHelp[message.args[0]])
			else:
				self.sendMsg(message.sender, 'usage: ' + self.remindersUsage)
		else:
			retVal = ['usage: ' + self.remindersUsage]
			retVal.append('The available reminder actions are: ' + ' '.join(self.remindersHelp.keys()))
			retVal.append('Your current reminders are:' if len(reminders) > 0 else u'You currently have no reminders. Use \u0002!remind\u0002 to add a reminder')
			for item in enumerate(reminders):
				parts = item[1][0].split()
				retVal.append(u'\u0002' + str(item[0] + 1) + u'\u0002: ' + timeUtils.getFriendlyTime(timeUtils.fromDatabase(parts[0])) + ' ' + ' '.join(parts[1:]))
			self.sendMultilineMsg(message.sender, retVal)

	def addReminder(self, user, timeDue, reminder, init = False):
		if user in self.reminders:
			numReminders = self.reminders[user]
			if numReminders >= self.maxReminders:
				return False
			self.reminders[user] = numReminders + 1
		else:
			self.reminders[user] = 1

		scheduler.addEvent(timeDue, [self.remindAndRemoveReminder, user, timeDue, reminder])
		
		if not init:
			database.addToPersistentStore(self.persistentType, user, timeUtils.toDatabase(timeDue) + ' ' + reminder)
		return True

	def remindAndRemoveReminder(self, user, timeDue, reminder):
		self.sendMsg(user, u'\u0002Reminder:\u0002 ' + reminder, 'reminders')
		database.deleteFromPersistentStore(self.persistentType, user, timeUtils.toDatabase(timeDue) + ' ' + reminder)
		numReminders = self.reminders[user] - 1
		if numReminders <= 0:
			del self.reminders[user]
		else:
			self.reminders[user] = numReminders

	def removeRemindersFromScheduler(self, reminders, user):
		self.reminders[user] -= len(reminders)
		for reminder in reminders:
			parts = reminder[0].split()
			timeDue = timeUtils.fromDatabase(parts[0])
			scheduler.removeScheduledEvent(timeDue, [self.remindAndRemoveReminder, user, timeDue, ' '.join(parts[1:])])

def __init__(levels):
	RemindHandler(levels)
