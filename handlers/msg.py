from handlerBase import HandlerBase, Command

class MsgHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!msg', self.handle))

	def handle(self, message):
		if message.numArgs >= 2:
			self.sendMsg(message.args[0], ' '.join(message.args[1:]))
		else:
			self.sendMsg(message.sender, 'usage: !msg <destination> <message>')

def __init__(levels):
	MsgHandler(levels)
