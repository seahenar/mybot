import config
from handlerBase import HandlerBase, Command

class DebugHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!debug', self.handle))

	def handle(self, message):
		config.debug = not config.debug
		self.sendMsg(message.sender, 'debug = ' + str(config.debug))

def __init__(levels):
	DebugHandler(levels)
