from datetime import datetime, timedelta

import database
import scheduler
import timeUtils
import channelTracker
from handlerBase import HandlerBase, Command

class StandupHandler(HandlerBase):

	def __init__(self, levels):
		self.dbTable = 'standup'
		self.dbColumns = '(channel text, time text)'
		database.createIfNotExists(self.dbTable, self.dbColumns)

		self.standupInitUsage = '!standupinit <time>'
		self.standupUsage = '!standup [<time> | cancel]'

		HandlerBase.__init__(self, levels, Command('!standupstop', self.handleStandupStop, 'Stop stand-up reminders for this channel', '!standupstop'))
		HandlerBase.__init__(self, levels, Command('!standupinit', self.handleStandupInit, 'Initiate stand-up reminders for this channel', self.standupInitUsage))
		HandlerBase.__init__(self, levels, Command('!standup', self.handleStandup, 'Allows you to cancel or modify the time of today\'s standup, on a one-off basis', self.standupUsage))

		self.exceptions = {}
		self.cancelled = []
		standups = database.execute('select channel from ' + self.dbTable, '').fetchall()
		for standup in standups:
			self.scheduleNextStandupReminder(standup['channel'])

	def handleStandupStop(self, message):
		if not message.isChannelDestination():
			message.reply('You must write this message in the channel you wish to disable Standups for')
			return
		if self.standupsNotEnabled(message.destination):
			message.reply('This channel does not have Standups enabled. Use !standupinit to do this.')
			return

		self.clearCurrentScheduledStandup(message.destination)
		database.execute('delete from ' + self.dbTable + ' where channel = ?', (message.destination,))
		self.sendMsg(message.destination, 'Standups have been disabled for this channel')
		
	def handleStandupInit(self, message):
		if message.numArgs < 1:
			message.reply(['You have not specified the time for the stand up meetings to occur', 'Usage: ' + self.standupInitUsage])
			return

		if not message.isChannelDestination():
			message.reply('You must write this message in the channel you wish to enable Standups for')
			return

		time = self.tryParseTime(message.sender, message.args[0])
		if not isinstance(time, datetime):
			message.reply(time)
		timeString = time.strftime('%H%M')

		self.clearCurrentScheduledStandup(message.destination)
		database.execute('delete from ' + self.dbTable + ' where channel = ?', (message.destination,))
		database.execute('insert into ' + self.dbTable + ' values (?, ?)', (message.destination, timeString))
		self.scheduleNextStandupReminder(message.destination)

		self.sendMsg(message.destination, u'This channel will now receive \u0002!standup\u0002 notifications at %s every week day' % message.args[0])
	
	def standupsNotEnabled(self, channel):
		return len(database.execute('select * from ' + self.dbTable + ' where channel = ?', (channel,)).fetchall()) == 0
	
	def handleStandup(self, message):
		if not timeUtils.isWorkingDay():
			message.reply('It is the weekend.  If there is a Standup today then something is very wrong')
			return

		if not message.isChannelDestination():
			message.reply('You must write this message in the channel where Standups are enabled')
			return

		if self.standupsNotEnabled(message.destination):
			message.reply('This channel does not have Standups enabled. Use !standupinit to do this.')
			return

		if message.numArgs < 1:
			nextDate = self.getNextStandupDateTime(message.destination)
			self.sendMsg(message.destination, 'The next standup is %s at %s' % (timeUtils.dayDifference(nextDate), nextDate.strftime('%H%M')))

		elif message.args[0] == 'cancel':
			self.clearCurrentScheduledStandup(message.destination)
			self.cancelled.append(message.destination)
			self.sendMsg(message.destination, 'Today\'s Standup has been cancelled')
			self.scheduleNextStandupReminder(message.destination)
		else:
			time = self.tryParseTime(message.sender, message.args[0])
			if not isinstance(time, datetime):
				message.reply(time)
				return
			if not timeUtils.isToday(time):
				self.sendMsg(message.destination, 'You have picked a time that would move the standup to tomorrow. Standups can only be rearranged to later on the same day.')
				return
			self.clearCurrentScheduledStandup(message.destination)
			self.exceptions[message.destination] = message.args[0]
			self.scheduleNextStandupReminder(message.destination)
			self.sendMsg(message.destination, 'Today\'s Standup has been moved (one-off) to %s' % message.args[0])

	def clearCurrentScheduledStandup(self, channel):
		if self.standupsNotEnabled(channel):
			return
		reminderTime = self.getNextStandupDateTime(channel)
		scheduler.removeScheduledEvent(reminderTime - timedelta(minutes = 5) , [self.issueStandupReminder, channel])
		scheduler.removeScheduledEvent(reminderTime - timedelta(seconds = 30), [self.issueStandupNotification, channel])
		self.clearCurrentExceptionStandup(channel)

	def scheduleNextStandupReminder(self, channel, alwaysTomorrow = False):
		reminderTime = self.getNextStandupDateTime(channel, alwaysTomorrow)
		scheduler.addEvent(reminderTime - timedelta(minutes = 5) , [self.issueStandupReminder, channel])
		scheduler.addEvent(reminderTime - timedelta(seconds = 30), [self.issueStandupNotification, channel])

	def getStandupParticipants(self, channel):
		return ', '.join(channelTracker.getUserList(channel))

	def issueStandupReminder(self, channel):
		self.sendMsg(channel, 'Reminder: Standup in 5 minutes ' + self.getStandupParticipants(channel))

	def issueStandupNotification(self, channel):
		self.clearCurrentExceptionStandup(channel)
		if not self.standupsNotEnabled(channel):
			self.inform(channel, 'Standup time! ' + self.getStandupParticipants(channel))
			self.scheduleNextStandupReminder(channel, True)

	def clearCurrentExceptionStandup(self, channel):
		if channel in self.exceptions.keys():
			del self.exceptions[channel]
		if channel in self.cancelled:
			self.cancelled.remove(channel)

	def getNextStandupDateTime(self, channel, alwaysTomorrow = False):
		date = timeUtils.getNextWorkingDay() if channel in self.cancelled or alwaysTomorrow == True else datetime.now()
		timeString = self.getTimeForStandupInChannel(channel)
		time = timeUtils.getTimeInFuture(timeString)

		print 'time', time
		print 'now', date
		dt = timeUtils.merge(date, time)
		print 'merged', dt
		if not isinstance(dt, datetime):
			print 'not dt'
			date = timeUtils.getNextWorkingDay()
			print 'next day', date
			dt = timeUtils.merge(date, time)
			print 'merged again', dt

		return dt

	def getTimeForStandupInChannel(self, channel):
		if channel in self.exceptions.keys():
			return self.exceptions[channel]

		return database.execute('select time from ' + self.dbTable + ' where channel = ?', (channel,)).fetchone()[0]
		
def __init__(levels):
	StandupHandler(levels)
