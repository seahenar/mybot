import config
import irc
from handlerBase import HandlerBase, Command

class KickHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!kick', self.handle))

	def handle(self, message):
		if message.numArgs >= 1:
			reason = 'admin requested'
			if message.numArgs >= 2:
				reason = ' '.join(message.args[1:])
			irc.kickUser(message.args[0], config.channel, reason)
		else:
			self.sendMsg(message.sender, 'usage: !kick <user> [reason]')

def __init__(levels):
	KickHandler(levels)
