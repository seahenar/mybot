import config
from handlerBase import HandlerBase, Command
from datetime import datetime, timedelta

class ModHandler(HandlerBase):

	def __init__(self, levels):
		self.triggerIntervalMins = 15
		self.lastTriggered = None

		HandlerBase.__init__(self, levels, Command('!mod', self.handle, 'If you think there is a problem in the channel and want to attract the attention of a moderator, you can use this command. You can optionally give a message, which will be relayed to all online moderators', '!mod [message]'))
		HandlerBase.__init__(self, levels, Command('!committee', self.handle, 'If you want to attract the attention of a committee member, you can use this command. You can optionally give a message, which will be relayed to all online committee members', '!committee [message]'))

	def handle(self, message):
		if message.destination != config.channel and message.destination != config.nick:
			return

		if self.lastTriggered != None and self.lastTriggered > datetime.now() - timedelta(minutes = self.triggerIntervalMins):
			self.sendMsg(message.sender, 'This command is currently unavailable: it has been used recently')
		else:
			self.lastTriggered = datetime.now()
			helpRequest = message.sender + ' requests help'
			if message.numArgs >= 1:
				helpRequest = helpRequest + ' (reason: ' + ' '.join(message.args[0:]) + ')'

			informHandler = self.getHandler('inform')
			
			if message.command == '!mod':
				informHandler.informPotentialUserGroup('moderator', helpRequest)
				self.sendMsg(message.sender, 'Moderators have been alerted to your problem, and should review the situation shortly')
			elif message.command == '!committee':
				informHandler.informPotentialUserGroup('committee', helpRequest)
				self.sendMsg(message.sender, 'Committee have been messaged, and should be with you shortly')

def __init__(levels):
	ModHandler(levels)
