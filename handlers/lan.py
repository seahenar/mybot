import config
import constants
import scheduler
from handlerBase import HandlerBase, Command

class LanHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!lan', self.handleLan, 'This tells you when and where our next LAN event is, along with the current signup count', '!lan'))
		HandlerBase.__init__(self, levels, Command('!meal', self.handleMeal, 'This tells you when and where our next Pre-LAN meal is, and how many people are going.', '!meal'))
		
		constants.addUpdateSource('http://www.cogsbristol.co.uk/signup/botinfo/')

	def handleLan(self, message):
		out = [constants.getConstant('lanInfo')]
		if self.isAuthedUserAtLevel(message.sender, 'committee'):
			out.append(constants.getConstant('lanInfoExtended'))

		self.sendMultilineMsg(message.sender, out)
			
	def handleMeal(self, message):
		self.sendMsg(message.sender, constants.getConstant('mealInfo'))

def __init__(levels):
	LanHandler(levels)
