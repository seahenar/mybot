import codecs
import config
import os
from datetime import datetime
from handlerBase import HandlerBase, Command
import handler

class LoggerHandler(HandlerBase):

	def __init__(self, level, chatLogDirectory):
		HandlerBase.__init__(self, level, Command('!rollover', self.handleRollover))

		self.chatLogDirectory = chatLogDirectory
		print 'Chat logs will be written to', os.path.abspath(self.chatLogDirectory)
		self.logFile = self.getLogFileHandle()
		self.start()
		handler.registerGlobalHandler('logger', self)

	def handleRollover(self, message):
		self.rollover()

	def getLogFileHandle(self):
		logDirectory = self.chatLogDirectory
		if not os.path.exists(logDirectory):
			os.makedirs(logDirectory)

		counter = 0
		getNextLogFileName = lambda: os.path.join(self.chatLogDirectory, 'log') + '.' + str(counter) + '.log'

		while os.path.isfile(getNextLogFileName()):
			counter += 1

		return codecs.open(getNextLogFileName(), 'a', 'utf-8')

	def log(self, message):
		if message != '':
			self.logFile.write(str(datetime.now()) + ' ' + message + '\n')

	def logBotQuit(self, quitReason):
		self.logFile.write(str(datetime.now()) + ' :' + config.nick + '!~bawt@helium.lan QUIT :' + quitReason + '\n')

	def logBotMessage(self, message):
		self.logFile.write(str(datetime.now()) + ' :' + config.nick + '!~bawt@helium.lan PRIVMSG ' + config.channel + ' :' + message + '\n')

	def start(self):
		self.logFile.write('connected\n')

	def stop(self):
		self.logFile.write('disconnected\n')
		self.logFile.flush()

	def rollover(self):
		self.logFile.write('rollover\n')
		self.logFile.flush()
		self.logFile = self.getLogFileHandle()
		self.logFile.write('rollover\n')

def __init__(arguments):
	LoggerHandler(arguments['level'], arguments['chatLogDirectory'] if 'chatLogDirectory' in arguments else 'logs/chat')
