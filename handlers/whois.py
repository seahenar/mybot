import config
import userTracker
from handlerBase import HandlerBase, Command
import re

class WhoisHandler(HandlerBase):

	def __init__(self, levels):
		self.usage = '!whois <nickname>'
		HandlerBase.__init__(self, levels, Command('!whois', self.handle, u'Retrieve user-defined information about a person (such as a Facebook link). You can set your own \u0002!whois\u0002 entry using \u0002!set whois\u0002', self.usage))

	def getWhois(self, who):
		entry = self.getHandler('userPrefs').getSetting('whois', who)
		if entry != 'not set':
			return entry
		return u'without an entry in my \u0002!whois\u0002 records. Tell them they can use \u0002!set whois [theirInfoHere]\u0002 to add it!'

	def handle(self, message):
		if message.numArgs == 1:
			if message.args[0].lower() == config.nick.lower():
				self.sendMsg(message.sender, 'I can be whatever you want me to be')
			else:
				msg = [message.args[0] + ' is ' + self.getWhois(message.args[0])]
				groups = self.getHandler('usergroups').getUserGroups(message.args[0])
				if len(groups) > 0:
					suffix = 's' if len(groups) > 1 else ''
					groups = ', '.join(groups)
					msg.append(message.args[0] + ' is in the ' + groups + ' group' + suffix)
				status = userTracker.getUserStatusText(message.args[0])
				if status != None:
					msg.append(message.args[0] + ' ' + status)
				self.sendMultilineMsg(message.sender, msg)
		else:
			msgs = ['usage: ' + self.usage]
			msgs.append(u'You can set your personal whois entry using \u0002!set whois\u0002')
			self.sendMultilineMsg(message.sender, msgs)
		
def __init__(levels):
	WhoisHandler(levels)
