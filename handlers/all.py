import channelTracker
from handlerBase import HandlerBase, Command

class AllHandler(HandlerBase):

	def __init__(self, levels):
		self.usage = '!all <channel> message'
		HandlerBase.__init__(self, levels, Command('!all', self.handle, 'Highlight everyone in a channel with a given message', self.usage))

	def handle(self, message):
		if message.numArgs < 1:
			message.reply('You must provide a message and optionally a channel. Usage: ' + self.usage)
			return
		if message.args[0].startswith('#'):
			channel = message.args[0]
			userMessage = ' '.join(message.args[1:])
		elif message.isChannelDestination():
			channel = message.destination
			userMessage = ' '.join(message.args[0:])
		else:
			message.reply('You must provide the channel or write this message in the channel in which you want to highlight everyone. Usage: ' + self.usage)
			return

		if not channel in channelTracker.getCurrentChannelList():
			message.reply('The bot is not in channel %s, either ask an administrator to add it to that channel, or choose a different channel' % channel)
			return

		self.sendMsg(channel, self.getNicksToHighlight(channel) + ': ' + userMessage)

	def getNicksToHighlight(self, channel):
		return ', '.join(channelTracker.getUserList(channel))

def __init__(levels):
	AllHandler(levels)
