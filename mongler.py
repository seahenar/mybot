import config
import random
import re

phase1 = re.compile('[aeiou](\w|\s)', re.I)
phase2 = re.compile('[aeiou]', re.I)
scooby = re.compile('(scooby)', re.I)

def shouldMongle():
	return config.enableRandomTypos and random.randint(0, config.averageErrorPer) == 0

def mongle(input):
	mongleToPerform = random.randint(0, len(monglers) - 1)
	return monglers[mongleToPerform](input)

def doMongle(input):
	correction = getCorrection(input) if phase2.search(input) else None
	temp = input.replace('ia', '0y0')
	temp = input.replace('io', '0y0')
	temp = temp.replace('ea', '0y0')
	temp = phase1.sub('0\\1', temp)
	return (phase2.sub('', temp), correction)

def doTypo(input):
	words = input.split()
	indexToTypo = random.randint(0, len(words) - 1)
	originalWord = words[indexToTypo]
	typoToInsert = typo(originalWord)
	if typoToInsert != None:
		words[indexToTypo] = typoToInsert
	return (' '.join(words), getCorrection(originalWord) if typoToInsert != None else None)
	
def doHashAtEnd(input):
	words = input.split()
	lastWord = words.pop()
	words.append(lastWord + '#')
	return (' '.join(words), getAnnoyance())

def doMissHomeKey(input):
	hand = random.randint(0, 1)
	thresholdIndex = len(keys[0]) / 2
	direction = random.randint(0, 1)
	direction = [2] if direction == 0 else [7]

	output = ''
	upper = False
	for char in input:
		if hand == 0 and direction == [2]:
			if char == 'A' or char == 'a':
				upper = not upper
				continue

		charLocation = getIndexOfChar(char)
		if charLocation != None:
			if hand == 0 and charLocation[1] < thresholdIndex or hand == 1 and charLocation[1] > thresholdIndex - 1:
				newChar = moveDirection(charLocation[0], charLocation[1], direction)
				newChar = newChar.upper() if upper else newChar.lower()
				output = output + newChar
				continue
		output = output + char.upper() if upper else output + char.lower()
	return (output, getCorrection(input))

def typo(word):
	if word in commonTypos.keys():
		return commonTypos[word]
	return getTypoType()(word)

def misTypeCharacter(word):
	indexToTypo = random.randint(0, len(word) - 1)
	newCharacter = makeTypo(word[indexToTypo])
	if newCharacter == None:
		return None
	word = word[0:indexToTypo] + newCharacter + word[indexToTypo + 1:]
	return word

def insertExtraCharacter(word):
	indexToTypo = random.randint(0, len(word) - 1)
	newCharacter = makeTypo(word[indexToTypo])
	if newCharacter == None:
		return None
	word = word[0:indexToTypo + 1] + newCharacter + word[indexToTypo + 1:]
	return word

def transposeCharacters(word):
	if len(word) < 2:
		return None
	indexToTypo = random.randint(0, len(word) - 2)
	newWord = word[0:indexToTypo] + word[indexToTypo + 1] + word[indexToTypo]
	if len(word) > indexToTypo + 1:
		newWord = newWord + word[indexToTypo + 2:]
	return newWord

def getCorrection(word):
	return word if word.startswith('*') else '*' + word

def getAnnoyance():
	annoyanceToGet = random.randint(0, len(annoyances) - 1)
	return annoyances[annoyanceToGet]

def getTypoType():
	return typos[random.randint(0, len(typos) - 1)]

def colourize(message):
	return scooby.sub(u'\u000313\\1\u0003', message)

def makeTypo(char):
	charLocation = getIndexOfChar(char)
	if charLocation == None:
		return None
	newChar = getTypoLocation(charLocation[0], charLocation[1])
	if char.isupper():
		return newChar.upper()
	return newChar

def getTypoLocation(i, j):
	if j == 0:
		if i == 0:
			return moveDirection(i, j, [5, 7, 8])
		if i == len(keys) - 1:
			return moveDirection(i, j, [4, 6, 7])
		else:
			return moveDirection(i, j, [4, 5, 6, 7, 8])
	elif j == len(keys[0]) - 1:
		if i == 0:
			return moveDirection(i, j, [2, 3, 5])
		if i == len(keys) - 1:
			return moveDirection(i, j, [1, 2, 4])
		else:
			return moveDirection(i, j, [1, 2, 3, 4, 5])

	if i == 0:
		return moveDirection(i, j, [2, 3, 5, 7, 8])
	elif i == len(keys) - 1:
		return moveDirection(i, j, [1, 2, 4, 6, 7])
	else:
		return moveDirection(i, j, [1, 2, 3, 4, 5, 6, 7, 8])

def moveDirection(i, j, options):
	direction = options[random.randint(0, len(options) - 1)]
	if direction == 1:
		return keys[i - 1][j - 1]
	if direction == 2:
		return keys[i][j - 1]
	if direction == 3:
		return keys[i + 1][j - 1]
	if direction == 4:
		return keys[i - 1][j]
	if direction == 5:
		return keys[i + 1][j]
	if direction == 6:
		return keys[i - 1][j + 1]
	if direction == 7:
		return keys[i][j + 1]
	if direction == 8:
		return keys[i + 1][j + 1]

def getIndexOfChar(char):
	for i in range(0, len(keys)):
		for j in range(0, len(keys[i]) - 1):
			if keys[i][j] == char.lower():
				return (i, j)

monglers = [doMongle, doTypo, doTypo, doTypo, doTypo, doTypo, doHashAtEnd, doHashAtEnd, doMissHomeKey]
typos = [misTypeCharacter, insertExtraCharacter, transposeCharacters]
commonTypos = {
'you\'re': 'your',
'its': 'it\'s',
'lose': 'loose',
'there': 'their',
'they\'re': 'their',
'we\'re': 'were'
}
annoyances = ['gah', 'blah', 'damn keyboard', '...stupid enter key', 'meh']
keys = [['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '='],
		['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']'],
		['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '#'],
		['\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', '']]
